public class MainLampu {
    public static void main(String[] args) {
        Lampu lampuDisco = new Lampu();
    
        // Nyalakan lampu kamar
        System.out.println("Apakah Lampu Dinyalakan?");
        lampuDisco.hidupkan();
    
        // Matikan lampu kamar
        System.out.println("Apakah Lampu Dinyalakan?");
        lampuDisco.matikan();
    }
}
